﻿USE [master]
GO
/****** Object:  Database [FamilyTree]    Script Date: 12/14/2018 3:42:53 PM ******/
CREATE DATABASE [FamilyTree]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FamilyTree', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\FamilyTree.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'FamilyTree_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\FamilyTree_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [FamilyTree] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FamilyTree].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FamilyTree] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FamilyTree] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FamilyTree] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FamilyTree] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FamilyTree] SET ARITHABORT OFF 
GO
ALTER DATABASE [FamilyTree] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FamilyTree] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [FamilyTree] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FamilyTree] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FamilyTree] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FamilyTree] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FamilyTree] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FamilyTree] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FamilyTree] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FamilyTree] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FamilyTree] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FamilyTree] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FamilyTree] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FamilyTree] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FamilyTree] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FamilyTree] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FamilyTree] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FamilyTree] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FamilyTree] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FamilyTree] SET  MULTI_USER 
GO
ALTER DATABASE [FamilyTree] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FamilyTree] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FamilyTree] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FamilyTree] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [FamilyTree]
GO
/****** Object:  Table [dbo].[Person]    Script Date: 12/14/2018 3:42:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Gender] [bit] NOT NULL,
	[Dad_id] [int] NULL,
	[Mom_id] [int] NULL,
	[Address] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Person] ON 

GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (1, N'Duy', 1, 2, 3, N'HCM')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (2, N'Ba', 1, 4, 5, N'HCM')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (3, N'Me', 0, 7, 8, N'HCM')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (4, N'Noi', 1, NULL, NULL, N'BT')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (5, N'Noi', 0, NULL, NULL, N'BT')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (7, N'Ngoai', 1, NULL, NULL, N'PT')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (8, N'Ngoai', 0, NULL, NULL, N'PT')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (9, N'Chau', 1, 1, 2, N'HCM')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (10, N'Ngoc', 0, 1, 2, N'HCM')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (11, N'Co', 0, 4, 5, N'BT')
GO
INSERT [dbo].[Person] ([id], [Name], [Gender], [Dad_id], [Mom_id], [Address]) VALUES (12, N'Chu', 1, 4, 5, N'BT')
GO
SET IDENTITY_INSERT [dbo].[Person] OFF
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD FOREIGN KEY([Dad_id])
REFERENCES [dbo].[Person] ([id])
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD FOREIGN KEY([Mom_id])
REFERENCES [dbo].[Person] ([id])
GO
USE [master]
GO
ALTER DATABASE [FamilyTree] SET  READ_WRITE 
GO
